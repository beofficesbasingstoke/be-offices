Flexible workspace for 1-100 desks in a premium office environment. Our unique all-inclusive workspace package comes with super-fast internet, no deposits and a service excellence guarantee. Choose from shared space, private office suites and the most flexible serviced offices in Basingstoke.

Address: ViewPoint, Basing View, Basingstoke, Hampshire RG21 4RG, United Kingdom

Phone: +44 800 917 4444

Website: https://www.beoffices.com/servicedoffice-locations/south-east/basingstoke

